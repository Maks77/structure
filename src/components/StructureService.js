import Vue from 'vue';
import {employees} from '../libs/employees.js';

export default new Vue({
	data() {
		return {
			data: employees,
			searched: '',
		}
	},
	methods: {
		add(newObj) {
			this.data.push(newObj)
		},
		onDetails(person) {
			this.$emit('onDetails', person, 'details-mode');
			console.log('emit', person)
		}

	},
	

})