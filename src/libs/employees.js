export let employees = [
	
	{	
		id: 1,
		subdivision: 'IT',
		name: "Алексей",
		surname: "Сорокопуд",
		patronymic: "Иванович",
		post: "начальник",
		mail: '40puds@gmail.com'
	},
	{	
		id: 2,
		subdivision: 'IT',
		name: "Вадим",
		surname: "Колобчак",
		patronymic: "Петрович",
		post: "специалист",
		mail: 'kolobok@gmail.com'
	},
	{	
		id: 3,
		subdivision: 'Legal Department',
		name: "Юлия",
		surname: "Кратенко",
		patronymic: "Викторовна",
		post: "начальник",
		mail: 'kratenko.y@gmail.com'
	},
	{	
		id:4,
		subdivision: 'Legal Department',
		name: "Вадим",
		surname: "Таболин",
		patronymic: "Игоревич",
		post: "специалист",
		mail: 'tabolin@gmail.com'
	},
	{
		id: 5,
		subdivision: 'Bookkeeping',
		name: "Анна",
		surname: "Антонюк",
		patronymic: "Васильевна",
		post: "начальник",
		mail: 'antoniyk@gmail.com'
	},
	{
		id: 6,
		subdivision: 'Bookkeeping',
		name: "Людмила",
		surname: "Бублик",
		patronymic: "Сергеевна",
		post: "ведущий специалист",
		mail: 'bublo@gmail.com'
	},
	{
		id: 7,
		subdivision: 'IT',
		name: "Иван",
		surname: "Сидоров",
		patronymic: "Сидорович",
		post: "младший специалист",
		mail: 'sidorovich@gmail.com'
	},
	{
		id: 8,
		subdivision: 'Legal Department',
		name: "Василий",
		surname: "Васильчук",
		patronymic: "Васильевич",
		post: "специалист",
		mail: 'vasua@gmail.com'
	},
]
